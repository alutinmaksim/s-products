<?php

class DB
{

    private string $host = 'localhost';
    private string $db = 'test_task';
    private string $user = 'root';
    private string $password = '';
    private string $charset = 'utf8';

    private function connect(): object
    {
        $dsn = "mysql:host=$this->host;dbname=$this->db;charset=$this->charset";
        $opt = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ];
        try {
            return new PDO($dsn, $this->user, $this->password, $opt);
        } catch (PDOException $e) {
            die("Не удалось подключиться: " . $e->getMessage());
        }
    }

    public function writeLogsToDB(): void
    {
        if (file_exists('log.txt')) {
            $file = fopen('log.txt', 'r');
            while (($line = fgets($file)) !== false) {
                $line = explode(';', $line);
                try {
                    $pdo = $this->connect();
                    $pdo->beginTransaction();
                    $query = 'INSERT INTO logs (search_query, number_of_products_found, date)
    VALUES (?, ?, ?)';
                    $stmt = $pdo->prepare($query);
                    $stmt->execute([$line[0], $line[1], $line[2]]);
                    $pdo->commit();
                } catch (PDOException $e) {
                    $pdo->rollBack();
                    echo "Ошибка:" . $e->getMessage();
                }
            }
            fclose($file);
        }
    }

    public function search($request): array
    {
        try {
            $query = 'SELECT title, price, quantity, SUM(quantity) OVER() AS q FROM products WHERE title LIKE ? AND quantity != 0';
            $stmt = $this->connect()->prepare($query);
            $stmt->execute(["%{$request}%"]);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            return array("Ошибка:" . $e->getMessage());
        }
    }

    public function populateDbWithData(): void
    {
        try {
            $query = "INSERT INTO `products` (`id`, `title`, `price`, `quantity`) VALUES (NULL, 'Iphone 15', '70000', '10'), (NULL, 'Iphone 15 pro', '100000', '8'), (NULL, 'Samsung S23', '65000', '12'), (NULL, 'Xiaomi 14', '90000', '5'), (NULL, 'Xiaomi 13C', '11000', '20'), (NULL, 'Redmi Pad SE', '16000', '21'), (NULL, 'Xiaomi Pad 6', '35000', '8'), (NULL, 'Honor x9b', '29000', '3'), (NULL, 'Honor 90 LITE', '23000', '11'), (NULL, 'realme 12 pro', '40000', '6')";
            $stmt = $this->connect()->prepare($query);
             $stmt->execute();
        } catch (PDOException $e) {
            echo "Ошибка:" . $e->getMessage();
        }
    }
}