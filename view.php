<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css">
    <title>Goods</title>
</head>
<body>

<div class="container col-6">
    <div class="">
        <div class="input-group mb-3">
            <input type="text" id="search" class="form-control" placeholder="введите наименование товара"
                   aria-label="Recipient's username" aria-describedby="button-addon2" value="" name="search">
            <button class="btn btn-outline-secondary" type="button" id="button-addon2">Найти</button>
        </div>
    </div>
    <button type="button" class="btn btn-outline-info" id="button-log" name="log">Сохранить логи в бд</button>

    <div class="container">
        <div id="products"></div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="app.js"></script>
</body>
</html>