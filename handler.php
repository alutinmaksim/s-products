<?php

require_once "DB.php";

if ($_POST) {
    $request = file_get_contents('php://input');
    $request = htmlspecialchars(trim($request));
    $db = new DB();
    $products = $db->search($request);
    if ($products) {
        $quantityProductsFound = $products[0]['q'];
        $date = date("Y-m-d H:i:s");
        $filename = __DIR__ . '/log.txt';
        $text = $request . ';' . $quantityProductsFound . ';' . $date;
        file_put_contents($filename, $text . PHP_EOL, FILE_APPEND);
    }
    echo json_encode($products);
}

if (isset($_GET['log'])) {
    $db = new DB();
    $db->writeLogsToDB();
    file_put_contents('log.txt', "");
}
