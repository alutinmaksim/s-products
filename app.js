const search = document.getElementById('search');
const table = document.getElementById('products');
document.getElementById('button-addon2').onclick = searchProducts;
document.getElementById('button-log').onclick = writeLogsToDB;

async function searchProducts() {
    const request = search.value;
    const data = await fetch('handler.php', {
        method: 'post',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: request
    });
    const res = await data.json();
    renderProducts(res, request);
    search.value = '';
}

function renderProducts(res, request) {
    let markup = "";
    const re = new RegExp(request, 'gi');
    const products = res.map(function (product) {
        return ` <tr>
        <td>${product.title.replace(re, `<span style="color: red">${request}</span>`)}</td>
        <td>${product.quantity}</td>
        <td>${product.price}</td>
    </tr>`
    });
    markup =
        `<table className="table table-striped">
    <thead>
    <tr>
        <th scope="col" class="mr-2">Наименование</th>
        <th scope="col">Количество</th>
        <th scope="col">Цена, руб</th>
    </tr>
    </thead>
    <tbody>
        ${products}
    </tbody>
</table>`
    if (products.length > 0) {
        table.innerHTML = markup;
    } else {
        table.innerHTML = 'Нет совпадений';
    }
}

async function writeLogsToDB() {
    await fetch('handler.php?' + new URLSearchParams({
        log: 'true'
    }));
}